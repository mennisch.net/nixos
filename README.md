# Introduction

This is my nixos configuration for my servers. There are currently three:

  - bastion
  - aurochs
  - ardennais

## bastion

This is an EC2 gateway machine with an Elastic IP running nginx proxying to other services on the VPN. The point of this instance is to run lean and expose little surface area to the Internet, while serving from a public IP other than my personal IP for a little bit of privacy shield.

The only external access is on ports 80 and 443 for nginx. SSH runs on 22 on the VPN, and I’ve sized the root EBS volume to 5GiB. Given than the swap is 1GiB and `/nix` is 1.7GiB, I believe this should provide enough room to do a full upgrade of all the packages.

It can be rebuilt as a t3a.nano machine using `configuration.nix` as user data, however, the nano does not have enough memory to do the nix build, so I have to manually add a swap to complete the build. The configuration includes a swap, so I create a swapfile in `/tmp` which is thrown away on reboot:

```
  dd if=/dev/zero of=/tmp/swapfile bs=1024 count=1000000
  chmod 0600 /tmp/swapfile
  mkswap /tmp/swapfile
  swapon /tmp/swapfile
  nixos-rebuild switch
```

In addition to `configuration.nix`, there are other git controlled files here. This repo should be checked out at `/`.

### Proxied services

I’m particularly interested in self-hosting federated services (in priority order):

  - goodreads (Bookwyrm)
  - microblogging (Mastodon or Pleroma, or something else)
  - a Twitter mirror (BirdsiteLive)
  - link aggregator (lemmy)
  - chat (matrix)
  - blogging (WriteFreely or Plume)
  - YouTube (PeerTube)
  - Instagram (pixelfed?)

I’d also like to self-host non-federated services:
  - an RSS reader (tt-rss, Newsblur, FreshRSS)
    - FreshRSS seems like it has some fediverse support?
  - read-it-later (Wallabag)

My plan is to run these on several raspis or whatever.
